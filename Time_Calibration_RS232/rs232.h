#ifndef _RS232_H_
#define _RS232_H_
#endif

#define READ_BUF_SIZE 512
#define READ_TIMEOUT 5000			////ms

#include <windows.h>

class rs232_module{
public:
	char lpBuf[READ_BUF_SIZE];

public:
	rs232_module();
	void set_para();
	void reveice();
	void char_reveice();
	void ReadFun();
	int transfer(char* lpBuf, DWORD dwToWrite);
};