#include "rs232.h"
#include "stdio.h"
#include "windows.h"
#include "string"
#include <iostream>
#include "stdlib.h"

using namespace std;

static HANDLE hComm;
static OVERLAPPED osReader = {0};
static OVERLAPPED osWrite = {0};

static DWORD drRead;
static DWORD drRes;
static BOOL fWaitingOnRead;

static DWORD dwWritten;
static DWORD dwRes;
static BOOL fRes;

static string comNum;

rs232_module::rs232_module()
{
	DCB dcb;
	//cout <<"It's use for check timer count of MCU \n ";
	//cout <<"Enter the port (ex: 20): ";
	//cin >> comNum;

	string comPrefix = "\\\\.\\COM"+comNum;
	//LPCTSTR comID=comPrefix.c_str();

	hComm = CreateFile(L"\\\\.\\COM12",GENERIC_READ | GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,NULL);
	if (hComm == INVALID_HANDLE_VALUE)
		printf("ERROR To Open COM12 \n");

	if(GetCommState(hComm, &dcb))
	{
		dcb.BaudRate = CBR_115200;
		dcb.ByteSize = 8;
		dcb.Parity = NOPARITY;
		dcb.StopBits = ONESTOPBIT;
		dcb.fBinary = true;

		if(SetCommState(hComm,&dcb))
			printf("DCB Set Fine \n");
	}
	else
		printf("GET/SET DCB Error!!!! \n");

	osReader.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);		// Create the read overlapped event. Must be closed before exiting to avoid a handle leak.
	if (osReader.hEvent == NULL)
		printf("Read: Creat event error \n");


	osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);		// Create this write operation's OVERLAPPED structure's hEvent.
	if (osWrite.hEvent == NULL)
		printf("Write: Creat event error \n");		// error creating overlapped event handle


	fWaitingOnRead=FALSE;
	lpBuf[0]=0;
}

void rs232_module::reveice()
{

	if (!fWaitingOnRead) 
	{
		if (!ReadFile(hComm, lpBuf, READ_BUF_SIZE, &drRead, &osReader)) 
		{
			int check =GetLastError();
			if (GetLastError() != ERROR_IO_PENDING)     // read not delayed?
				printf("Read error !=ERROR_IO_PENDING : %d \n",check);
			else
			{
				printf("IO_PENDING \n");
				fWaitingOnRead = TRUE;
			}
		}
		else 
		{
			for(int i=0; i<(int)drRead;i++)
				printf("Received: %c \n ",lpBuf[i]);
		}
		//	HandleASuccessfulRead(lpBuf, dwRead);
	}
	else
	{
		drRes = WaitForSingleObject(osReader.hEvent, READ_TIMEOUT);
		switch(drRes)
		{
			// Read completed.
		case WAIT_OBJECT_0:
			if (!GetOverlappedResult(hComm, &osReader, &drRead, FALSE))
				printf("Overlap Error \n");				// Error in communications; report it.
			else
			{
				// Read completed successfully.
				for(int i=0; i<(int)drRead;i++)
					printf("Received: %c \n ",lpBuf[i]);
			}

			//HandleASuccessfulRead(lpBuf, dwRead);

			//  Reset flag so that another opertion can be issued.
			fWaitingOnRead = FALSE;
			break;

		case WAIT_TIMEOUT:
			// Operation isn't complete yet. fWaitingOnRead flag isn't
			// changed since I'll loop back around, and I don't want
			// to issue another read until the first one finishes.
			//
			// This is a good time to do some background work.
			printf("Read Timeout Occur !!!! \n");
			break;                       

		default:
			// Error in the WaitForSingleObject; abort.
			// This indicates a problem with the OVERLAPPED structure's
			// event handle.
			break;
		}
	}

}

void rs232_module::char_reveice()
{

	if (!fWaitingOnRead) 
	{
		if (!ReadFile(hComm, lpBuf, 1, &drRead, &osReader)) 
		{
			int check =GetLastError();
			if (GetLastError() != ERROR_IO_PENDING)     // read not delayed?
				printf("Read error !=ERROR_IO_PENDING : %d \n",check);
			else
			{
				printf("IO_PENDING \n");
				fWaitingOnRead = TRUE;
			}
		}
		else if(lpBuf[0]!=0)
		{
			printf("Received: %c \n ",lpBuf[0]);
		}
		//	HandleASuccessfulRead(lpBuf, dwRead);
	}
	else
	{
		drRes = WaitForSingleObject(osReader.hEvent, READ_TIMEOUT);
		switch(drRes)
		{
			// Read completed.
		case WAIT_OBJECT_0:
			if (!GetOverlappedResult(hComm, &osReader, &drRead, FALSE))
				printf("Overlap Error \n");				// Error in communications; report it.
			else
			{
					printf("Received: %c \n ",lpBuf[0]);
			}

			//HandleASuccessfulRead(lpBuf, dwRead);

			//  Reset flag so that another opertion can be issued.
			fWaitingOnRead = FALSE;
			break;

		case WAIT_TIMEOUT:
			// Operation isn't complete yet. fWaitingOnRead flag isn't
			// changed since I'll loop back around, and I don't want
			// to issue another read until the first one finishes.
			//
			// This is a good time to do some background work.
			printf("Read Timeout Occur !!!! \n");
			break;                       

		default:
			// Error in the WaitForSingleObject; abort.
			// This indicates a problem with the OVERLAPPED structure's
			// event handle.
			break;
		}
	}

}

int rs232_module::transfer(char* wpBuf, DWORD dwToWrite)
{

	// Issue write.
	if (!WriteFile(hComm, wpBuf, dwToWrite, &dwWritten, &osWrite)) 
	{
		if (GetLastError() != ERROR_IO_PENDING)				// WriteFile failed, but isn't delayed. Report error and abort.
			fRes = FALSE;
		else													// Write is pending.
		{
			dwRes = WaitForSingleObject(osWrite.hEvent, INFINITE);
			switch(dwRes)
			{
				// OVERLAPPED structure's event has been signaled. 
			case WAIT_OBJECT_0:
				if (!GetOverlappedResult(hComm, &osWrite, &dwWritten, FALSE))
					fRes = FALSE;
				else
					// Write operation completed successfully.
					fRes = TRUE;
				break;

			default:
				// An error has occurred in WaitForSingleObject.
				// This usually indicates a problem with the
				// OVERLAPPED structure's event handle.
				fRes = FALSE;
				break;
			}
		}
	}
	else
		// WriteFile completed immediately.
		fRes = TRUE;

	return fRes;
}


void rs232_module::ReadFun()
{
    DWORD    dwEv;
    DWORD    dwRead;
    char    input;
    dwRead = 1;
    while (1)
    {
        WaitCommEvent(hComm, &dwEv, NULL);
        if (dwEv == EV_RXCHAR)
        {
            if (!ReadFile(hComm, &input, dwRead, &dwRead, &osReader))
            {
                //cout << "Read Error" << endl;
                //break;
            }
            printf("%c",input);
        }
    }
}