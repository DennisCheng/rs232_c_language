#include "stdio.h"
#include "rs232.h"
#include "time.h"
#include <iostream>

using namespace std;
using namespace System::Threading;

void main()
{
	rs232_module port_12;

	clock_t start,stop;

	//Thread receive_thread= gcnew Thread(port_12.reveice());

	//receive_thread.Start();

	char dev_cmd[10]={0x31,0x36,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x36};
	char key[12]={0x30,0x30,0x30,0x39,0x30,0x30,0x30,0x30,0x31,0x36,0x32,0x37};
	
	while(1)
	{
		int command;
		char Check_ready=0;
		char C0_flag=0;
		char C0_coount=0;
		char tt;

		cout<<"Select Test Function: \n\n";
		cout<<"0)Scan Time interval \n";
		cout<<"1)UART Timeout check \n";
		cout<<"2)Unlock Key delay time check \n";
		cout<<"3)Sleep time check \n";
		cout<<"8)BMS Virtual TEST 0 \n";
		cout<<"9)BMS Virtual TEST 1 \n";
		cout<<"10)BMS Virtual TEST 2 \n";
		cin>>command;

		switch(command)
		{
		case 0:
			port_12.lpBuf[0]=0;
			start=0;
			while(1)
			{
				port_12.char_reveice();
				if(port_12.lpBuf[0]==0x0b)
				{
					if(!C0_flag)
						start=clock();
					C0_flag=1;
				}
				else if(C0_flag==1 && port_12.lpBuf[0]==0x0f)
				{
					C0_flag=2;
					C0_coount++;
				}

				if(C0_coount==100)
				{
					stop=clock();
					break;
				}
			}
			break;
		case 1:
			port_12.lpBuf[0]=0;
			start=0;
			tt=0x01;
			port_12.transfer(&tt,1);
			start=clock();
			while(1)
			{
				port_12.char_reveice();
				if(port_12.lpBuf[0]==0x55)
				{
					stop=clock();
					port_12.lpBuf[0]=0;
					break;
				}
				else
					port_12.lpBuf[0]=0;
			}
			break;
		case 2:
			port_12.lpBuf[0]=0;
			start=0;
			C0_flag=0;

			tt=0x01;
			port_12.transfer(&tt,1);

			for(int i=0;i<10;i++)
				port_12.transfer(dev_cmd+i,1);

			for(int i=0;i<12;i++)
				port_12.transfer(key+i,1);

			tt=0x03;
			port_12.transfer(&tt,1);
			while(1)
			{
				port_12.char_reveice();
				if(port_12.lpBuf[0]==0x4B)
				{
					start=clock();
					C0_flag=1;
				}
				else if(C0_flag==1 && port_12.lpBuf[0]==0x4C)
				{
					stop=clock();
					break;
				}
				port_12.lpBuf[0]=0;
			}
			break;
		case 3:
			port_12.lpBuf[0]=0;
			start=0;
			C0_flag=0;
			while(1)
			{
				port_12.char_reveice();
				if(port_12.lpBuf[0]==0x53)
				{
					start=clock();
					C0_flag=1;
				}
				else if(C0_flag==1 && port_12.lpBuf[0]==0x54)
				{
					stop=clock();
					break;
				}
				port_12.lpBuf[0]=0;
			}
			break;
		case 8:
			tt=0x01;
			port_12.transfer(&tt,1);
			tt=0x56;
			port_12.transfer(&tt,1);
			tt=0x44;
			port_12.transfer(&tt,1);
			tt=0x30;
			port_12.transfer(&tt,1);
			tt=0x03;
			port_12.transfer(&tt,1);
			break;
		case 9:
			tt=0x01;
			port_12.transfer(&tt,1);
			tt=0x56;
			port_12.transfer(&tt,1);
			tt=0x44;
			port_12.transfer(&tt,1);
			tt=0x31;
			port_12.transfer(&tt,1);
			tt=0x03;
			port_12.transfer(&tt,1);
			break;
		case 10:
			break;
		}


		if(start!=0 && stop!=0)
		{
			float time_interval=float((stop-start))/CLOCKS_PER_SEC;
 			printf("time interval is : %f",time_interval);
		}

	}
}